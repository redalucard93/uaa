/**
 * View Models used by Spring MVC REST controllers.
 */
package com.vinja.uaa.web.rest.vm;
